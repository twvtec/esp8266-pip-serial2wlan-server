# ESP8266-PIP-Serial2Wlan-Server

ESP8266 based Serial2WLAN-Server to control MPPsolar PIP Inverter series

## Install

- `git clone https://bitbucket.org/twvtec/esp8266-pip-serial2wlan-server`
- Move files to your projects folder
- `git clone https://github.com/vinmenn/Crc16` [Download ZIP](https://github.com/vinmenn/Crc16/archive/master.zip)
- Move library to your libraries folder and restart IDE to register the library

## Usage

- Set your local SSID and password

```cpp
//WiFi Config
const char* ssid = "yourSSID";
const char* password = "yourPassword";
```
- Compile and upload it 


## Request example

- [**POST**] `/v1?cmd=QPI`
- [**RESPONSE**] `(PI30<CRC><CR>`


## Command example
**QPI** - Request the device protocol ID
 
```
Request: <ESPIpAddress>/v1?cmd=QPI
Response: (PI<NN><CRC><cr>
N is an integer number ranging from 0 to 9
Protocol ID distribution: 30 for HS/MS/MSX HS series 
```
	 
For all the other possible command please refer to protocol documentation


## Author
[Tobias Wenken](mailto:tobias@wenken.de)