//Deps
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Crc16.h>

//Init Classes
ESP8266WebServer server(80);
Crc16 crc;

//WiFi Config
const char* ssid = "yourSSID";
const char* password = "yourPassword";

//Var declaration
byte  crcTxBuf[2];
char  serResBuf[200];




// Calculating CRC16-Checksum for rxCmd
// Increments crcTxBuf for high or low byte if checksum is
//    0x28 -> reserved as startbyte for device response
//    0x0D -> <CR> End of message
//    0x0A -> <LF> 
//
// @param {byte[200]} data    data for calculation of checksum
byte* calcCrc(byte data[200]) {
  
  unsigned short crcValue = crc.XModemCrc(data,0,String((char*) data).length());
  crcTxBuf[1] = crcValue & 255;
  crcTxBuf[0] = (crcValue >> 8)  & 255;
  if((crcTxBuf[0] == 0x28) || (crcTxBuf[0] == 0x0D) || (crcTxBuf[0] == 0x0A)){
    crcTxBuf[0]++;
  }
  if((crcTxBuf[1] == 0x28) || (crcTxBuf[1] == 0x0D) || (crcTxBuf[1] == 0x0A)){
    crcTxBuf[1]++;
  }
  
  return crcTxBuf;  
  
}

// Checks if device response is correct
// No validation of CRC-checksum send by device
//
//@param {byte[200]} res     response data read from serial port
int verifySerialResponse(byte res[200]) {
  // Start byte is "("
  if (res[0] == 0x28) {
    
    // Response is "NAK" for incorrect request
    if((res[1] == 0x4E) && (res[2] == 0x41) && (res[3] == 0x4B)) {
      return 400;
    } else {
        
    //Response OK; Response body not checked
      return 200;
    }
  } else {
    
    // Request not valid; Bad Request
    return 500;
  }
  
}


// Check if rxCmd is a valid command
//
// @param {byte[200]} cmd     Command received by HTTP-Post request
bool validateRxCmd(byte cmd[200]) {
  String tmpCmd = String((char*)cmd);
  if (
      //Inquire Commands
      ((tmpCmd.substring(0,3) == "QPI")       && (tmpCmd.length() == 3)) ||       //Device protocol ID inquiry
      ((tmpCmd.substring(0,3) == "QID")       && (tmpCmd.length() == 3)) ||       //The device serial number inquiry
      ((tmpCmd.substring(0,4) == "QVFW")      && (tmpCmd.length() == 4)) ||       //Main CPU firmware version inquiry
      ((tmpCmd.substring(0,5) == "QVFW2")     && (tmpCmd.length() == 5)) ||       //Another CPU firmware version inquiry 
      ((tmpCmd.substring(0,5) == "QPIRI")     && (tmpCmd.length() == 5)) ||       //Device rating information inquiry
      ((tmpCmd.substring(0,5) == "QFLAG")     && (tmpCmd.length() == 5)) ||       //Device flag status inquiry
      ((tmpCmd.substring(0,5) == "QPIGS")     && (tmpCmd.length() == 5)) ||       //Device general status parameters inquiry
      ((tmpCmd.substring(0,4) == "QMOD")      && (tmpCmd.length() == 4)) ||       //Device mode inquiry
      ((tmpCmd.substring(0,5) == "QPIWS")     && (tmpCmd.length() == 5)) ||       //Device warning status inquiry
      ((tmpCmd.substring(0,3) == "QDI")       && (tmpCmd.length() == 3)) ||       //Device default setting value information
      ((tmpCmd.substring(0,7) == "QMCHGCR")   && (tmpCmd.length() == 7)) ||       //Device selectable value about max charging current inquiry
      ((tmpCmd.substring(0,8) == "QMUCHGCR")  && (tmpCmd.length() == 8)) ||       //Device selectable value about max utility charging current inquiry
      ((tmpCmd.substring(0,5) == "QBOOT")     && (tmpCmd.length() == 5)) ||       //Device DSP has bootstrap or not inquiry
      ((tmpCmd.substring(0,4) == "QOPM")      && (tmpCmd.length() == 4)) ||       //Device output mode inquiry (For 4k/5k)
      ((tmpCmd.substring(0,5) == "QPGSn")     && (tmpCmd.length() == 5)) ||       //Device parallel information inquiry（For 4k/5k）

      //Setting parameters Commands
      ((tmpCmd.substring(0,2) == "PE") && (tmpCmd.substring(5,3) == "/PD")) ||    //Setting some status enable/disable
      (tmpCmd.substring(0,2) == "PF") ||                                          //Setting control parameter to default value
      (tmpCmd.substring(0,1) == "F") ||                                           //Setting device output rating frequency
      (tmpCmd.substring(0,3) == "POP") ||                                         //Setting device output source priority
      (tmpCmd.substring(0,4) == "PBCV") ||                                        //Setting battery re-charge voltage
      (tmpCmd.substring(0,4) == "PBDV") ||                                        //Setting battery re-discharge voltage
      (tmpCmd.substring(0,3) == "PCP") ||                                         //Setting device charger priority
      (tmpCmd.substring(0,3) == "PGR") ||                                         //Setting device grid working range
      (tmpCmd.substring(0,3) == "PBT") ||                                         //Setting battery type
      (tmpCmd.substring(0,4) == "PSDV") ||                                        //Setting battery cut-off voltage (Battery under voltage)
      (tmpCmd.substring(0,4) == "PCVV") ||                                        //Setting battery C.V. (constant voltage) charging voltage
      (tmpCmd.substring(0,4) == "PBFT") ||                                        //Setting battery float charging voltage
      (tmpCmd.substring(0,5) == "PPVOKC") ||                                      //Setting PV OK condition
      (tmpCmd.substring(0,4) == "PSPB") ||                                        //Setting Solar power balance
      (tmpCmd.substring(0,5) == "MCHGC") ||                                       //Setting max charging current
      (tmpCmd.substring(0,6) == "MUCHGC") ||                                      //Setting utility max charging current
      (tmpCmd.substring(0,4) == "POPM") ||                                        //Setting output mode (For 4000/5000)
      (tmpCmd.substring(0,4) == "PPCP")                                           //Setting parallel device charger priority (For 4000/5000)
      ) {
        return true;
  } else {
    return false;
  }  
}

// Sending command to device
//
// @param {byte[200]} cmd
char* sendCommand(byte cmd[200]) {
  Serial.write((char*) cmd);
  
  // Calculating checksum for cmd and send it out
  Serial.write(calcCrc(cmd),2);
  
  // Terminating
  Serial.write(0x0D);

  // Clear serRefBuf
  // Receiving response from device
  memset(serResBuf, 0, 200);
  Serial.readBytesUntil(0x0D, serResBuf, 200);

  return serResBuf;
}

// Handling incoming HTTP-Post requests
void requestHandler() {
  byte  rxCmd[200];

  //Getting first arg from URL as rxCmd
  server.arg(0).getBytes(rxCmd, sizeof(rxCmd));
    
  // Validating and handling rxCmd
  // Checking if rxCmd is a valid command
  if( validateRxCmd(rxCmd) ){

    char* serialResponse = sendCommand(rxCmd);
    int httpStatus = verifySerialResponse( (byte*) serialResponse );

    if(httpStatus == 200) {
      server.send(200, "text/plain", (char*) serialResponse);
    } else {
      server.send(httpStatus, "text/plain", "Error");
    }  
  } else {
    server.send(404, "text/plain", "Command not found");
  }
}

void setup() {
  //Init UART für PIP
  Serial.begin(2400);

  //Connecting to local WiFi
  WiFi.begin(ssid, password);
  
  // Waiting while connecting
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);  
  }

  // Defining request-URL
  server.on("/v1", HTTP_POST, requestHandler);

  // Starting server
  server.begin();
}

void loop() {
  server.handleClient();
}
